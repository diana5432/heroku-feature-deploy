module RidesHelper
    def ride_update(ride)
        if current_user==ride.user
            ride.passengers=ride.passengers
        else
            if ride.passengers&.include? current_user.id
                ride.passengers.delete current_user.id
                ride.passengers=ride.passengers
            else
                if ride.passengers.nil?
                    ride.passengers = Array.new
                end
                if ride.passengers.size < ride.max_passengers
                    ride.passengers=ride.passengers.push current_user.id
                else
                    ride.passengers=ride.passengers
                end
            end
        end
    end
end