class RidesController < ApplicationController
    def new
        @user = User.find(params[:user_id])
        @ride = @user.rides.build
    end

    def create
        @user = User.find(params[:user_id])
        @ride = @user.rides.create(ride_params)

        if @ride.valid?
            @ride.save
            redirect_to user_path(@user)
        else
            render :new
        end
    end

    def destroy
        @user = User.find(params[:user_id])
        @ride = @user.rides.find(params[:id])
        @ride.destroy

        redirect_to user_path(@user)
    end

    def edit
        @ride = Ride.find(params[:id])
        @user = @ride.user
    end

    def update
        @ride = Ride.find(params[:id])
        @user = @ride.user
        @ride.passengers = ride_update(@ride)
        if @ride.update(ride_params)
            redirect_to current_user
        else
            render :edit
        end
    end
    
    def index
        @rides = Ride.all
    end

    def show
        @ride = Ride.find(params[:id])
    end

    private
    def ride_params
        params.require(:ride).permit(:city_from, :city_to, :date, :time, :max_passengers, {:passengers => []})
    end
end
