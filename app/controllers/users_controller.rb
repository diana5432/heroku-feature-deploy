class UsersController < ApplicationController
  def show
    @user = User.find(params[:id])
    @rides = Ride.all
  end

  def user_params
    params.require(:user).permit(:name, :email)
  end 
end
