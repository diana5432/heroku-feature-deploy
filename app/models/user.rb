class User < ApplicationRecord
    has_many :rides, dependent: :destroy
    accepts_nested_attributes_for :rides
end
