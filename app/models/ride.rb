class Ride < ApplicationRecord
  belongs_to :user

  validates :city_from, presence: true, length: { in: 2..60 }
  validates :city_to, presence: true, length: { in: 2..60 }
  validates :date, presence: true
  validate :date_of_ride_cannot_be_in_the_past
  validates :max_passengers, presence: true, numericality: { greater_than: 0, less_than_or_equal_to: 10 }

  private
    def date_of_ride_cannot_be_in_the_past
      if date.present? && date < Date.today
        errors.add(:date, "can't be in the past")
      end
    end

end
