### Feature description

## Modelling Rides with Passengers

Die Models User und Rides werden hinzugefügt. 
Das Model Rides ermöglicht das Anbieten von und das Teilnehmen an Fahrten. Fahrten (rides) haben dabei eine\*n Fahrer\*in (user) und 1 bis n Mitfahrer\*innen (andere users). 
Ein sehr vereinfachtes User Model mit prototypischem Login, welches nur die Email-Adresse benötigt, wurde hinzugefügt, um die Abhängigkeit des Models Rides darzustellen. Eingeloggte User können dabei eigene Fahrten anbieten, editieren und löschen. An Fahrten anderer User können sie teilnehmen oder ihre Teilnahme zurückziehen. So wird verhindert, dass sie als Anbieter einer Fahrt (= Fahrer*in) selbst auch noch als Gast daran teilnehmen.
Die CRUD-Methoden werden gemäß REST-Paradigma angelegt. 

# Heroku Link

Folgt.
