class CreateRides < ActiveRecord::Migration[6.0]
  def change
    create_table :rides do |t|
      t.references :user, null: false, foreign_key: true
      t.string :city_from
      t.string :city_to
      t.date :date
      t.time :time
      t.integer :max_passengers
      t.integer :passengers, array: true

      t.timestamps
    end
  end
end
