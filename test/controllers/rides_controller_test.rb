require 'test_helper'

class RidesControllerTest < ActionDispatch::IntegrationTest
  def setup
    @user = User.new(name: "ExampleUser", email: "example@user.com")
    @ride = Ride.new(user: @user, city_from: "ExampleFrom", city_to: "ExampleTo",
    date: Date.today, max_passengers: 3)
    @user.save
    @ride.save
  end

  test "should get new" do
    get new_user_ride_path @user
    assert_response :success
  end

  test "should get edit" do
    get edit_user_ride_path @user, @ride
    assert_response :success
  end

  test "should get index" do
    get rides_path 
    assert_response :success
  end

  test "should get show" do
    get ride_path @ride
    assert_response :success
  end
end
