require 'test_helper'

class RideTest < ActiveSupport::TestCase
  def setup
    @user = User.new(name: "ExampleUser", email: "example@user.com")
    @ride = Ride.new(user: @user, city_from: "Berlin", city_to: "Potsdam",
                     date: Date.today, max_passengers: 3)
  end

  test "should be valid" do
    assert @ride.valid?
  end

  test "cities should be present" do
    @ride.city_from = "    "
    assert_not @ride.valid?
    @ride.city_from = "aa"
    assert @ride.valid?
    @ride.city_to = "      "
    assert_not @ride.valid?
    @ride.city_to = "bb"
    assert @ride.valid?
  end

  test "cities should not be too short" do
    @ride.city_from = "a"
    assert_not @ride.valid?
    @ride.city_from = "aa"
    assert @ride.valid?
    @ride.city_to = "b"
    assert_not @ride.valid?
    @ride.city_to = "bb"
    assert @ride.valid?
  end

  test "cities should not be too long" do
    @ride.city_from = "a" * 61
    assert_not @ride.valid?
    @ride.city_from = "a" * 60
    assert @ride.valid?
    @ride.city_to = "b" * 61
    assert_not @ride.valid?
    @ride.city_to = "b" * 60
    assert @ride.valid?
  end

  test "date should be present" do
    @ride.date = "    "
    assert_not @ride.valid?
  end

  test "date should not be in the past" do
    @ride.date = Date.today - 1 
    assert_not @ride.valid?
  end

  test "max_passengers should be present" do
    @ride.max_passengers = "    "
    assert_not @ride.valid?
  end

  test "max_passengers should be between 1 and 10" do
    @ride.max_passengers = 0
    assert_not @ride.valid?
    @ride.max_passengers = 11
    assert_not @ride.valid?
    @ride.max_passengers = 1
    assert @ride.valid?
    @ride.max_passengers = 10
    assert @ride.valid?
  end
end


